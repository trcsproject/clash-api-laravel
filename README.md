# Clash of Clans API Integration

## Overview

This project was created to integrate with the official Clash of Clans APIs. To make requests, you will need a valid API token. The main objective of this project is to store queries to maintain historical data.

## Project Structure

- **app**
  - **Domains**: This directory contains business logic, where methods from repository classes and other rules are called to return the expected results.
  - **Http/Controllers**: This directory contains controllers that will call the methods from the service interfaces.
  - **Integrations**: This directory contains integration classes, which handle request/response operations only, without saving objects.
  - **Integrations/Interfaces**: This directory contains the integration interfaces.
  - **Models**: This directory contains the project's models along with their relationships.
  - **Providers**: This directory contains the project's providers.
  - **Repository**: This directory contains classes that handle database alterations without any business logic. If data needs to be processed before being saved to the database, this processing should be done in the domain. If it is something that might be used throughout the project, it should be placed in Utils.
  - **Services**: This directory contains orchestrator classes that manage calls between integrations and domains. Here, responses from integrations are received and passed to domains to apply business rules and save to the database if necessary.

## Installation

To install the project, follow these steps:

1. **Clone the repository**:
   ```sh
   git clone https://gitlab.com/trcsproject/clash-api-laravel.git
   ```

2. **Ensure you have the following versions**:

   - PHP 8.2
   - Composer 2.0+
   - Laravel version 11
   - MySQL for the database

3. **Navigate to the project directory**:
   ```sh
   cd clash-api-laravel
   ```

4. **Install dependencies**:
   ```sh
   composer install
   ```

5. **Create a new database** for the project.

6. **Update the .env file**:
   Add the following environment variables to your `.env` file:
   ```env
   CLAN_TAG=2GUC9JJR8
   CLASH_OF_CLANS_API_KEY=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiIsI...
   ```

7. **Run migrations**:
   After updating the `.env` file, run the following command to create the necessary database tables:
   ```sh
   php artisan migrate
   ```
8. **Run**
   
   To retrieve the records, simply access the configured routes in the `routes` file. 
   
   Example:
   ```php
   path-to-your-project/clan/{clanTag}

   
## Scheduler

The scheduler is configured to handle a single clan, but it can be modified to reload data for all previously loaded clans.

### Setting Up Crontab

Add your cron job configuration here to automate tasks.

```sh
# Example crontab entry
* * * * * cd /path-to-your-project && php artisan schedule:run >> /dev/null 2>&1
```

Replace `/path-to-your-project` with the actual path to your project directory.
